console.log("Hello World");
/*
	FUNCTIONS
		functions in javascript are lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked

		functions are mostly created to creat complicated tasks to run several lines of code in succession

		they are also used to prevent repeating lines/blocks of codes that perform the same task/function
*/

// FUNCTION DECLARATION
/*
	FUNCTION keyword - defines a function in javascript; indicator that we are creating a function that will be invoked later in our codes

	printname() - functionName; functions are named to be able to use later in the code
	function block {} - the statements inside the curly brace which comprise of the body of the function, this is where the codes are to be executed

		SYNTAX
			function functionName(){
				function  statements/code block;
			}
*/
function printName(){
	console.log("My name is John");
};

// call/invoke the function
/*
	the code block and statements inside a function is not immediately executed when the function is defined/declared. the code block/statements inside a function is executed when the function is invoked/called.

	it is common to use the term "call a function" instead of "invoke a function"
*/
printName();

// results in an error due to non-existence of the function(the function is not declared)
// declaredFunction();

// DECLARATION v EXPRESSION
// FUNCTION DECLARATION - using function keyword and functionName
declaredFunction();  //functions can be hoisted
/*
	functions in javascript can be used before it can be defined, thus hoisting is allowed for the javascript functions
		NOTE: hoisting is javascripts default behavior for certain variable and functions to run/use them before their declaration
*/
function declaredFunction(){
	console.log("Hello from declaredFunction");
};

// FUNCTION EXPRESSION
/*
	a function can also be created by storing it inside a variable

	does not allow hoisting

	a function expression is an anonymous function that is assigned to the variableFunction
		anonymous function - unnamed function

	variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error since technically it is a variable.
*/
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

function topAnime(){
	console.log("Attack on Titan, SpyxFamily, Gundam ")
}
topAnime();

function topMovie(){
	console.log("MCU, Harry Potter film series, Avatar");
}
topMovie();

// REASSIGNING DECLARED FUNCTIONS
// we can also reassign declared functions and function expressions to new anonymous functions, however, we cannot change the declared functions/function expressions that are defined using const
declaredFunction=function(){
	console.log("updated declaredFunction");
};
declaredFunction();

const constFunction=function(){
	console.log("Initialize const function");
};
constFunction();

/*constFunction=function(){
	console.log("cannot be reassigned");
};
constFunction();*/

// FUNCTION SCOPING
/*
	scope is the accessibility of variables and functions

	Javascript variables/functions have 3 scopes
		1. local/block scope - can be accessed inside the curly brace/code block {}
		2. global scope - anything written in the .js file/outside curly braces
*/

{
	let localVar="Armando Perez";
	console.log(localVar);
}
let globalVar="Mr Worldwide";
console.log(globalVar);
// console.log(localVar);

/*
	function scope
		JS has also function scope; each function creates a new scope
		variables defined inside a function can only be accessed and used inside that function
		variables declared with var, let, and const are quite similar when declared inside a function
*/

function showNames(){
	// function-scoped variables
	var functionVar="Joe";
	const functionConst="John";
	let functionLet="Eme";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();
// 	console.log(functionVar);
// 	console.log(functionConst);
// 	console.log(functionLet);
/*
	functionVar, functionConst, functionLet are function-scoped and cannot be accessed outside the function they are delcared.
*/


// NESTED FUNCTIONS
/*
	functions that are defined inside another function, these nested functions have functions scope where they can ONLY be accessed inside the function where they are declared/defined
*/

function newFunction(){
	let name="Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name); // name is a default reference in js. but this is valiud since we are accessing it inside a nested function that is still inside the function where "name" is defined.
		console.log(nestedName);
	}
	// console.log(nestedName); returns an error since the nestedName has function scoping; it can only be accessed inside the nestedFunction
	nestedFunction();
}
newFunction();
// nestedFunction(); - returns an error since we must call the nestedFunction inside the function where it is declared

let globeVar="Hello Globe"
function funcTest(){
	let smartVar="Hello Smart";
	console.log(globeVar);
	console.log(smartVar);
};
funcTest();
// console.log(smartVar); returns an error - not defined

// USING ALERT()
/*
	alert() - allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our users, the page will wait /continue to load until the user dismisses the dialog
*/

// alert("Hello hello baby you called? I dont hear thing"); // will show as the page loads


// we can use an alert() to show a message to the user from a later function invocation
function showSampleAlert(){
	alert("I have got no service in the club, you see, see");
}
// showSampleAlert();
// we find that the page waits for the user to dismiss the dialog box before proceeding, we witness this by relaoding the pagfe while the console is open
console.log("I will display after the alert is closed")

/*
	Reminders/Notes on using alert();
		show only an alert for short dialogs/messages to the user. 
		do not overuse alert() because the program / js has to wait for the message to be dismissed to continue
*/

// USING PROMPT()
// prompt() - is used to allow us to show a small window at the top of the browser to gather user input. Much like alert(), it will havethe page wait until the user completes or enters the input. The input from the prompt() will be returned as as string data type once the user dismisses the window.
/*
	SYNTAX
		prompt("<dialogInString>");
*/

/*let samplePrompt=prompt("Enter Your Name");
console.log(typeof samplePrompt);
console.log("Hello "+samplePrompt);
*/

/*let nullPrompt=prompt("do not enter anything here, PLEASE");
console.log(nullPrompt);*/

// prompt() returns an empty string is user click OK button without enterng any information, and "null" for users who click CANCEL button
/*
	Reminders/Notes on using promp()
		it can be used to gather user data/inout and be used in our code. However, since it will have the page wait until the user finised or closed the window, it must not be overused.

		prompt() used globally will run immediately, so for better usage and user experience, it is better to store them inside a function and call whenever they are needed
*/


/*function welcomeMessage(){
	let firstN=prompt("Enter your first name");
	let lastN=prompt("Enter your last name");
	console.log("Hello "+firstN+" "+lastN);
}
welcomeMessage();*/

// FUNCTION NAMING
// function name convention should be definitive of the task that it will perform. it usually contains or starts with a verb/adjective - description/forming words
/*
	Reminders/Notes on naming convention
		1. definitive of the task that it will perform
		2. name your functions with small letters, in cases of multiple words, use camelcasing (underscore is also valid)
*/

function getCourses(){
	let courses=["Science 101","Math 101","English 101"];
	console.log(courses);
};
getCourses();

// we have to avoid generic names to avoid confusion within our codes
function get(){
	let nam="Jamie";
	console.log(nam);
};
get();

// avoid pointless / inappropriate name for our functions
function foo(){
	console.log(25%5);
};
foo();


